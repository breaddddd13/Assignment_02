var menuState = {
  preload: function(){
    game.load.image('score', 'assets/score.png');
  },
  create: function () {
    game.stage.backgroundColor = '#000000';

    const nameLabel = game.add.text(window_width / 2, window_height / 2 - 100, 'Raiden', {
      font: '40px Arial',
      fill: '#ffffff'
    });
    nameLabel.anchor.setTo(0.5, 0.5);

    const startLabel = game.add.text(window_width / 2, window_height / 2 + 100, 'Press enter to start', {
      font: '25px Arial',
      fill: '#ffffff'
    });
    startLabel.anchor.setTo(0.5, 0.5);

    game.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(this.start, this);
    this.scoreboard = game.add.button(window_width / 2, window_height / 2 + 200, 'score', this.showScore, this);
    this.scoreboard.anchor.setTo(0.5, 0.5);

  },
  start: function (){
    game.state.start('main');
  },

  showScore: function(){
    let result = '';
    let sortList = [];
    let list = firebase.database().ref('list');
    list.once('value').then(function(snapshot){

      let arr = snapshot.val();
      for(const key in arr) sortList.push(arr[key]);
      
      sortList.sort((x, y)=> y.score - x.score);
      for(let i = 0; i < 10; i++) result += `${sortList[i].name}: ${sortList[i].score}\n`;
      window.alert(result);
    })
    
  }
}
