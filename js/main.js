const game_width = 700;
const game_height = 700;

const window_width = 1050;
const window_height = 700;


var game = new Phaser.Game(window_width, window_height, Phaser.AUTO, 'canvas');
game.global = {};

game.state.add('menu', menuState);
game.state.add('main', mainState);
game.state.start('menu');

