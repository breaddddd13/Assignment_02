
let bulletTime = 0;
let score = 0;
let firingTimer = 0;
let cdFiring = 0;
let skillTime = 0;
let pushScore = false;

var mainState = {
  preload: function(){
    game.load.image('background', 'assets/background.png');
    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('heart', 'assets/heart.png');
    game.load.image('power', 'assets/star.png');
    game.load.image('enemyBullet', 'assets/enemyBullet.png');
    game.load.spritesheet('ship', 'assets/ship.png', 32, 32);
    game.load.spritesheet('explode', 'assets/explode.png', 128, 128);
    game.load.spritesheet('alien','assets/alien.png', 32, 32);

    game.load.audio('music', 'assets/music.mp3');
    game.load.audio('shot', 'assets/shoot.mp3');
  },
  create: function(){

    game.world.setBounds(0, 0, game_width - 20, game_height);

    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.input.keyboard.addCallbacks(this, this.resume, null);
    this.skillBut = game.input.keyboard.addKey(Phaser.Keyboard.X);
    this.cursor = game.input.keyboard.createCursorKeys();
    this.gg = false;
    this.background = game.add.tileSprite(0, 0, game_width, window_height, 'background');
    
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.createMultiple(30, 'bullet');
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 1);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);

    this.enemyBullets = game.add.group();
    this.enemyBullets.enableBody = true;
    this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.enemyBullets.createMultiple(30, 'enemyBullet');
    this.enemyBullets.setAll('anchor.x', 0.5);
    this.enemyBullets.setAll('anchor.y', 1);
    this.enemyBullets.setAll('outOfBoundsKill', true);
    this.enemyBullets.setAll('checkWorldBounds', true);

    this.aliens = game.add.group();
    this.aliens.enableBody = true;
    this.aliens.physicsBodyType = Phaser.Physics.ARCADE;

    this.ship = game.add.sprite(game_width / 2, game_height - 200, 'ship');
    this.ship.animations.add('right', [3, 4], 8, false);
    this.ship.animations.add('left', [2, 1], 8, false);
    this.ship.anchor.setTo(0.5, 0.5);
    game.physics.arcade.enable(this.ship);

    this.hearts = game.add.group();
    this.powers = game.add.group();
    this.emitters = game.add.group();

    for (var i = 0; i < 3; i++) {
      this.life = this.hearts.create(game_width + 100 + (32 * i), 200, 'heart');
      this.life.scale.setTo(0.7, 0.7);
      this.power = this.powers.create(game_width + 100 + (32 * i), 300, 'power');
    }

    this.music = game.add.audio('music', 1, true);
    this.music.play();
    this.mVolume = 100;
    
    this.shotEffect = game.add.audio('shot');
    this.sVolume = 100;
    game.sound.setDecodedCallback(this.shotEffect, this.update, this);
    game.add.text(game_width + 20, 500, 'Music: ', {
      font: '18px Arial',
      fill: '#ffffff'
    });

    this.mVolumeLabel = game.add.text(game_width + 70, 500, this.mVolume, {
      font: '18px Arial',
      fill: '#ffffff'
    });

    game.add.text(game_width + 20, 550, 'Sound Effect: ', {
      font: '18px Arial',
      fill: '#ffffff'
    });

    this.sVolumeLabel = game.add.text(game_width + 130, 550, this.sVolume, {
      font: '18px Arial',
      fill: '#ffffff'
    });
    this.scoreText = game.add.text(game_width + 90, 100, 'Score: ' + score, {
      font: '18px Arial',
      fill: '#FFFFFF'
    });
    this.gameoverLabel = game.add.text(game_width / 2 - 100, game_height / 2, 'Game Over\nPress Enter to Restart\nPress Esc to menu', {
      font: '25px Arial',
      fill: '#FFFFFF'
    });
    this.gameoverLabel.visible = false;
    game.add.text(game_width + 90, 150, 'Player : ',{
       font: '25px Arial',
       fill: '#FFFFFF'
    });
    game.add.text(game_width + 90, 250, 'Power : ', {
      font: '25px Arial',
      fill: '#FFFFFF'
    });
    game.add.text(game_width + 20, 400, 'Press ESC to Stop', {
      font: '25px Arial',
      fill: '#FFFFFF'
    });
    
  },
  update: function(){
    if(this.gg) this.gameOver();
    this.background.tilePosition.y += 1;
    this.addPower();
    this.moveShip();
    this.fireBullet();
    if (this.aliens.countLiving() == 0) this.createAliens();
    if (game.time.now > firingTimer) this.enemyFires();
    if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) this.restart();
    if (game.input.keyboard.isDown(Phaser.Keyboard.X)) this.shipSkill();
    if (game.input.keyboard.isDown(Phaser.Keyboard.ESC)) {
      if (this.gg) {
        this.music.stop();
        game.state.start('menu');
      } else {
        game.paused = true;
      }
    }
    if (game.input.keyboard.isDown(Phaser.Keyboard.Q)) this.musicVolume(0.003);
    if (game.input.keyboard.isDown(Phaser.Keyboard.W)) this.musicVolume(-0.003);
    if (game.input.keyboard.isDown(Phaser.Keyboard.A)) this.effectVolume(0.003);
    if (game.input.keyboard.isDown(Phaser.Keyboard.S)) this.effectVolume(-0.003);

    game.physics.arcade.overlap(this.bullets, this.aliens, this.collisionHandler, null, this);
    game.physics.arcade.overlap(this.ship, this.aliens, this.hitsPlayer, null, this);
    game.physics.arcade.overlap(this.ship, this.enemyBullets, this.hitsPlayer, null, this);

  },
  moveShip: function (){
    if (this.cursor.left.isDown && this.ship.body.x >= 0) {
      this.ship.x -= 10;  
      this.ship.animations.play('left');
    }
    else if (this.cursor.right.isDown && (this.ship.body.x + this.ship.width) <= game_width) {
      this.ship.x += 10;
      this.ship.animations.play('right');
    }
    else if (this.cursor.up.isDown && this.ship.body.y >= 0) this.ship.y -= 10;
    else if (this.cursor.down.isDown && (this.ship.body.y + this.ship.height) <= game_height) this.ship.y += 10;
    else this.ship.frame = 0;
  },
  fireBullet: function(){
  //  To avoid them being allowed to fire too fast we set a time limit
    if(!this.ship.alive) return;
    if (game.time.now > bulletTime) {
      //  Grab the first bullet we can from the pool
      bullet = this.bullets.getFirstExists(false);
      if (bullet) {
        this.shotEffect.play();
        //  And fire it
        bullet.reset(this.ship.x, this.ship.y + 8);
        bullet.body.velocity.y = -400;
        bulletTime = game.time.now + 200;
      }
    }
  },
  createAliens: function(){
    if (this.gg) return;
    if(score < 800){
      cdFiring = 2000;
      for(let y = 0; y < 4; y++) {
        for (let x = 0; x < 10; x++) {
          this.createAlien(100 + x * 48,50 + y * 50, 0, 0, 10);
        }
      }
      //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
      // game.add.tween(this.aliens).to({x: 200}, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    }else if(score >= 800 && score < 1500){
      cdFiring = 1000;
      for (let x = 0; x < 30; x++){
        const rand_x = game.rnd.integerInRange(0, game_width);
        const rand_vx = game.rnd.integerInRange(-10, 10);
        const rand_vy = game.rnd.integerInRange(50, 100);
        this.createAlien(rand_x, 0, rand_vx, rand_vy, 20);
      }
    }else if (score >= 1500) {
      cdFiring = 500;
      for (let x = 0; x < 20; x++) {
        const rand_x = game.rnd.integerInRange(0, game_width)
        const rand_vx = game.rnd.integerInRange(-10, 10);
        const rand_vy = game.rnd.integerInRange(20, 30);
        this.createAlien(rand_x, -50, rand_vx, rand_vy, 30);
      }
    }
  },
  createAlien: function (x, y, vx, vy, health) {
    const alien = this.aliens.create(x, y, 'alien');
    alien.body.velocity.x = vx;
    alien.body.velocity.y = vy;
    alien.health = health;
    alien.outOfBoundsKill = true;
    alien.checkWorldBounds = true;
    alien.anchor.setTo(0.5, 0.5);
    alien.animations.add('move', [0, 1, 2, 3], 8, true);
    alien.play('move');
    alien.body.moves = true;
  },
  collisionHandler: function(bullet, alien) {
    //  When a bullet hits an alien we kill them both
    if(alien.alive){
      bullet.kill();
      alien.damage(5);
    }
    if(!alien.alive){
      bullet.kill();
      alien.kill();
      this.emitter = this.emitters.getFirstExists(false);
      this.emitter = game.add.emitter(bullet.body.x, bullet.body.y, 15);
      this.emitter.makeParticles('pixel');
      this.emitter.setYSpeed(-150, 150);
      this.emitter.setXSpeed(-150, 150);
      this.emitter.setScale(2, 0, 2, 0, 800);
      this.emitter.start(true, 800, null, 15);
      //  Increase the score
      score += 20;
      this.scoreText.text = 'Score: ' + score;
    }
  },
  enemyFires: function(){
      //   //  Grab the first bullet we can from the pool
    if (this.gg) return;
    const enemyBullet = this.enemyBullets.getFirstExists(false);
    const livingEnemies = [];
  
    this.aliens.forEachAlive(function (alien) {
      // put every living enemy in an array
      livingEnemies.push(alien);
    });

    if (enemyBullet && livingEnemies.length > 0) {
      const random = game.rnd.integerInRange(0, livingEnemies.length - 1);
      // randomly select one of them
      const shooter = livingEnemies[random];
      // And fire the bullet from this enemy
      enemyBullet.reset(shooter.body.x, shooter.body.y);

      game.physics.arcade.moveToObject(enemyBullet, this.ship, 120);
      firingTimer = game.time.now + cdFiring;
    }
  }, 
  hitsPlayer: function (ship, bullet) {
    if (!this.ship.alive) return;
    bullet.kill();
    ship.kill();
    this.explode(this.ship.x, this.ship.y);

    this.life = this.hearts.getFirstAlive();
    if (this.life) {
      this.life.kill();
    }
    if (this.hearts.countLiving() == 0) {
      // this.gameoverLabel.text = ;
      this.gg = true;
    }else{
      this.ship.revive();
    }
  },
  explode: function (x, y) {
    const explosion = game.add.sprite(x, y, 'explode');
    explosion.anchor.setTo(0.5);
    explosion.animations.add('bang');
    explosion.play('bang', 10, false, true);
  },
  restart: function() {
    if(!this.gg) return;
    this.gameoverLabel.visible = false;
    this.hearts.callAll('revive');
    this.aliens.destroy(true, true);
    this.enemyBullets.forEach(i =>{
      i.kill();
    })
    this.bullets.forEach(bullet => {
      bullet.kill();
    });
    this.ship.revive();
    this.ship.x = game_width / 2;
    this.ship.y = game_height - 200;
    score = 0;
    this.scoreText.text = 'Score: ' + score;
    this.gg = false;
    bulletTime = 0;
    score = 0;
    firingTimer = 0;
    cdFiring = 0;
    pushScore = false;
  },
  resume: function(){
    if (!game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) return;
    game.paused = false;
  },
  shipSkill: function(){
    if(!this.ship.alive) return;
    if(this.powers.countLiving() == 0) return;
    if (skillTime > game.time.now) return;
    skillTime = game.time.now + 2000;

    this.power = this.powers.getFirstAlive();
    if(this.power) this.power.kill();
    let set = [];
    set[0] = game.add.sprite(this.ship.x, this.ship.y, 'bullet', 0, this.bullets);
    set[1] = game.add.sprite(this.ship.x, this.ship.y, 'bullet', 0, this.bullets);
    set[2] = game.add.sprite(this.ship.x, this.ship.y, 'bullet', 0, this.bullets);
    set[3] = game.add.sprite(this.ship.x, this.ship.y, 'bullet', 0, this.bullets);
    set[4] = game.add.sprite(this.ship.x, this.ship.y, 'bullet', 0, this.bullets);

    for (let i = 0; i < 5; i++) set[i].body.velocity.y = -1000;

    set[0].body.velocity.x = -200;
    set[0].angle = -40;
    set[1].body.velocity.x = -100;
    set[1].angle = -20;
    set[3].body.velocity.x = 100;
    set[3].angle = 20;
    set[4].body.velocity.x = 200;
    set[4].angle = 40;
  },
  addPower: function(){
    if(!this.ship.alive) return;
    if (score == 1000) this.powers.callAll('revive');
    else if (score == 2000) this.powers.callAll('revive');
    else if (score == 3000) this.powers.callAll('revive');
  },
  musicVolume: function (value){
    if (this.music.volume + value >= 1) value = 1 - this.music.volume;
    if (this.music.volume + value <= 0) value = 0 - this.music.volume;
    this.music.volume += value;

    this.mVolume = Math.round(this.music.volume * 100);
    this.mVolumeLabel.text = this.mVolume;
  },
  effectVolume: function(value){
    if (this.shotEffect.volume + value >= 1) value = 1 - this.shotEffect.volume;
    if (this.shotEffect.volume + value <= 0) value = 0 - this.shotEffect.volume;
    this.shotEffect.volume += value;

    this.sVolume = Math.round(this.shotEffect.volume * 100);
    this.sVolumeLabel.text = this.sVolume;
  },
  gameOver: function(){
    this.gameoverLabel.visible = true;
    if(!pushScore){
      const name = prompt("Please enter your name", "");
      const list = firebase.database().ref('list');
      list.push().set({
        name: name,
        score: score
      }).then(function(){
        score = 0;
      });
      pushScore = true;
    }
  }
}
