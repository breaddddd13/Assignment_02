# Software Studio 2018 Spring Raiden
> https://breaddddd13.gitlab.io/Assignment_02
## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete Game Process|15%|Y|
|Basic Rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|y|
|Particle Systems|10%|Y|
|Sound Effects|5%|Y|
|UI|5%|Y|
|Leader Board|5%|Y|
|Appearance|5%|Y|

## Bonus Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi-Player|20%|N|
|Enhance Items|15%|N|
|Boss|5%|N|

## Detail Description
* Basic Rules
    1. player : 按上下左右會移動，自動發射子彈
    2. enemy : 隨機產生，並且隨機發射攻擊以及降下。
    3. Map會隨著時間往上滾動。
    4. 按ESC會暫停，再按Enter會繼續。
* Jucify mechanisms
    1. 當玩家的分數到達800、1500分時，會產生第二、三級enemies，因此更難躲避敵人。
    2. 當我按下X鍵時，會發射很多子彈。
    3. 技能次數為3次，每1000分會補充次數。
* Animations
    1. player為船，並且會左右擺動。
    2. enemy為外星人，會有走路的動畫。
* Particle Systems
    1. 當子彈跟其他物品overlap時，會產生爆炸的特效。
* Sound Effects
    1. 增加了背景音效以及射子彈音效。
    2. 按下Q、W分別為增加背景音量以及降低背景音量。
    3. 按下A、S分別為增加子彈音量以及降低子彈音量。
* UI
    1. 顯示分數、生命值、技能次數
* Leaderboard
    1. 在menu可以選擇board，並且可以看到排名前十的玩家以及其分數。